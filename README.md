# conf-registration-api

Sinatra based API for conference registration
based on [Creating a dummy API using Sinatra](https://wamae.medium.com/creating-a-dummy-api-using-sinatra-775cc29ca399)

## Setup on CentOS8

Create a sudo user and update the server

```bash
sudo dnf -y update
```

Install dependencies

```bash
sudo dnf -y install epel-release yum-utils libcurl-devel
sudo yum-config-manager --enable epel
sudo dnf clean all
sudo dnf update -y
sudo dnf -y install libnsl2 chrony gpg gpgme python3-gpg gcc gcc-c++ make tar git
```

On a system with a 4Gb of Ram or less, it is helpful to
create swap space
```bash
sudo dd if=/dev/zero of=/swap bs=1M count=4096
sudo chmod 0600 /swap
sudo mkswap /swap
sudo swapon /swap
```
Make the swap space permanent
```bash
sudo vi /etc/fstab
```
add the line
```
/swap swap swap defualts 0 0
```
`
Then setup Network Protocol which uses Chrony in CentOS8
```bash
sudo systemctl enable chronyd
```

Then edit the configuration file to allow the system to act
as an NTP client
```bash
sudo vi /etc/chrony.conf
```
ensure the line
```bash
# Allow NTP client access from local network.
allow 10.10.0.0/24
```
is uncommented and ip.address is replaced
then restart Chrony
```bash
sudo systemctl restart chronyd
```
Check that the server is running
```bash
chronyc sources
```
This should give you an output similar to
```
210 Number of sources = 5
MS Name/IP address         Stratum Poll Reach LastRx Last sample
===============================================================================
^? 185-70-197-199.pl-waw1.u>     0   6     0     -     +0ns[   +0ns] +/-    0ns
^* ntp1.dnainternet.fi           2   6    17     2  -1599us[ -579us] +/-   29ms
^+ time.cloudflare.com           3   6    17     3  +2048us[+3068us] +/-   15ms
^+ gamma.rueckgr.at              2   6    17     2  -1553us[ -534us] +/-   51ms
^+ static.206.188.217.95.cl>     2   6    17     1  -1671us[-1671us] +/-   38ms
```
Then check the client
is running
```bash
sudo chronyc clients
```
which should indicate that your local server is a client
```
Hostname                      NTP   Drop Int IntL Last     Cmd   Drop Int  Last
===============================================================================
localhost                       0      0   -   -     -       6      0  -4    75
```
Use RVM to manage Ruby versions.  Get RVM keys
```bash
sudo gpg2 --keyserver hkp://keys.openpgp.org --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
```
If the above does not work, get the keys manually
```bash
command curl -sSL https://rvm.io/mpapis.asc | sudo gpg2 --import -
command curl -sSL https://rvm.io/pkuczynski.asc | sudo gpg2 --import -
```
then install rvm
```bash
curl -sSL https://get.rvm.io | sudo bash -s stable
```
Finally, enable the user to be able to use rvm
```bash
sudo usermod -a -G rvm `whoami`
```
Check if `secure_path` is configured and if so set `rvmsudo_secure_path=1`
```bash
if sudo grep -q secure_path /etc/sudoers; then sudo sh -c "echo export rvmsudo_secure_path=1 >> /etc/profile.d/rvm_secure_path.sh" && echo Environment variable installed; fi
```

Then logout of the server
```bash
exit
```
and log back in again to activate RVM
```bash
ssh username@ip.address
```

Then set a default ruby version, releases are listed at
https://www.ruby-lang.org/en/downloads/releases/
```bash
rvm install ruby-2.7.4
rvm --default use ruby-2.7.4
```

Check the default Ruby version
```bash
ruby --version
```
which may give output similar to
```
ruby 2.7.4p191 (2021-07-07 revision a21a3b7d23) [x86_64-linux]
```
Then install bundler
```bash
gem install bundler
```

Then enable the Phusion passenger repositories and install them

```bash
sudo curl --fail -sSLo /etc/yum.repos.d/passenger.repo https://oss-binaries.phusionpassenger.com/yum/definitions/el-passenger.repo
sudo dnf install -y nginx-mod-http-passenger || sudo yum-config-manager --enable cr && sudo dnf install -y nginx-mod-http-passenger
sudo dnf -y install passenger-devel
```
Restart Nginx to ensure it is running
```bash
sudo systemctl restart nginx
```
If Nginx does not start, it may be the case that a directory is missing. You will
need to create it and give permissions to the Nginx user, which is likely `nginx`,
then start Nginx
```bash
sudo mkdir -p /usr/share/nginx/passenger_temp
sudo chown nginx /usr/share/nginx/passenger_temp
sudo systemctl restart nginx
```
Then validate the installation
```bash
sudo /usr/bin/passenger-config validate-install
```

You should get the following output
```
What would you like to validate?
Use <space> to select.
If the menu doesn't display correctly, press '!'

 ‣ ⬢  Passenger itself
   ⬡  Apache

-------------------------------------------------------------------------

 * Checking whether this Passenger install is in PATH... ✓
 * Checking whether there are no other Passenger installations... ✓

Everything looks good. :-)
```

Now check that Nginx is running
```bash
sudo /usr/sbin/passenger-memory-stats
```

Which should give output similar to
```
Version: 6.0.10
Date   : 2021-08-07 12:35:04 +0000
------------- Apache processes -------------
*** WARNING: The Apache executable cannot be found.
Please set the APXS2 environment variable to your 'apxs2' executable's filename, or set the HTTPD environment variable to your 'httpd' or 'apache2' executable's filename.


---------- Nginx processes -----------
PID    PPID   VMSize    Private  Name
--------------------------------------
61236  1      127.4 MB  0.5 MB   nginx: master process /usr/sbin/nginx
61239  61236  155.1 MB  1.0 MB   nginx: worker process
### Processes: 2
### Total private dirty RSS: 1.46 MB


----- Passenger processes -----
PID    VMSize    Private  Name
-------------------------------
61221  364.6 MB  1.9 MB   Passenger watchdog
61224  650.5 MB  2.9 MB   Passenger core
### Processes: 2
### Total private dirty RSS: 4.87 MB
```

At this point, we can deploy our application. First create
a user for the application
```bash
sudo adduser appuser
```

Make a directory for the code
```bash
sudo mkdir -p /var/www/app
sudo chown appuser: /var/www/app
```
Change to the app user
```bash
sudo -u appuser -H bash -l
rvm use ruby-2.7.4
cd /var/www/app
```
obtain the code
```bash
git clone https://gitlab.com/nairuby/conf-registration-api .
```
install dependencies and return to sudo user
```bash
bundle config set --local deployment 'true'
bundle config set --local without 'development test'
bundle install
bundle pristine
exit
```
Check if FirewallD is running
```bash
sudo systemctl status firewalld
```
If it is, open a port for application, say 8793
```bash
sudo firewall-cmd --zone=public --permanent --add-port=8793/tcp
sudo firewall-cmd --reload
```
if not, use the IPtables/NFtables commands to open the port.
```bash
sudo iptables -I INPUT 2 -p tcp --dport 8793 -j ACCEPT
sudo dnf install iptables-services
sudo systemctl start iptables
sudo systemctl enable iptables
sudo service iptables save
```

Create an Nginx configuration file
```bash
sudo vi /etc/nginx/conf.d/app.conf
```
and put the following content in it
```
server {
  listen 8793;
  server_name ip.address;
  # Tell Nginx and Passenger where your app's 'public' directory is
  root /var/www/app/public;
  # Turn on Passenger
  passenger_enabled on;
  passenger_ruby /usr/local/rvm/gems/ruby-2.7.4/wrappers/ruby;
}
```
Check if SELinux is enabled
```bash
sudo sestatus
```
If the output contains `SELinux status:    enabled`, you will need to allow Nginx 
to use a non-standard port and allow Nginx to use the config.ru file,
```bash
sudo semanage port -a -t http_port_t -p tcp 8793
sudo semanage fcontext -a -t httpd_sys_content_t /var/www/app/config.ru
```
Test the application works
```bash
sudo systemctl restart nginx
curl localhost:8793
```
should give the output
```
Habari Dunia! Karibu jamii ya Ruby ya Afrika
```

Then enable Nginx to start on boot
```bash
sudo systemctl enable nginx
```

Check it is working from a remote system
```bash
curl ip.address:8793
```
should give the output
```
Habari Dunia! Karibu jamii ya Ruby ya Afrika
```


## References
- http://sinatrarb.com/intro.html
- https://www.phusionpassenger.com/docs/tutorials/deploy_to_production/
- https://artsysops.com/2019/03/05/how-to-create-or-remove-swap-file-in-centos-7/
- https://linuxconfig.org/redhat-8-open-http-port-80-and-https-port-443-with-firewalld/
- https://www.phusionpassenger.com/docs/advanced_guides/gls/generic_guide.html
- http://llawlor.github.io/2016/03/05/phusion-passenger-nginx-notes.html
- https://gist.github.com/arteezy/5d53d99f6ee617fae1f0db0576fdd418
- https://matthias-grosser.de/blog/2015/01/deploying-multiple-rails-apps-with-passenger-standalone-and-systemd/
- https://www.phusionpassenger.com/library/config/standalone/reference/
- https://www.phusionpassenger.com/library/deploy/config_ru.html#sinatra
- https://tommy.chheng.com/post/123568050761/deploying-a-sinatra-app-on-nginx-passenger-with
- https://github.com/phusion/passenger-ruby-sinatra-demo
- https://www.phusionpassenger.com/docs/tutorials/deploy_to_production/deploy_updates/
- https://pi.gadgetoid.com/article/ruby-with-nginx-passenger
- https://www.digitalocean.com/community/tutorials/how-to-install-rails-and-nginx-with-passenger-on-ubuntu
- http://codylittlewood.com/build-and-deploy-ruby-microsite-with-sinatra/
- https://tecadmin.net/deploy-ruby-app-nginx-passenger-ubuntu/
- https://blog.sudobits.com/2014/11/12/deploying-sinatra-applications-to-vps-with-passengernginx-ubuntu-14-04/
- https://computingforgeeks.com/how-to-install-gollum-wiki-on-ubuntu-18-04-lts/
- https://www.digitalocean.com/community/tutorials/how-to-deploy-sinatra-based-ruby-web-applications-on-ubuntu-13
- http://coding.jandavid.de/2016/02/08/how-to-set-up-sinatra-with-activerecord/
- https://guides.railsgirls.com/sinatra-app
- https://www.toptal.com/ruby/api-with-sinatra-and-sequel-ruby-tutorial
- https://blog.cloud66.com/deploying-rest-apis-to-docker-using-ruby-and-sinatra/
- https://wamae.medium.com/creating-a-dummy-api-using-sinatra-775cc29ca399
- https://www.eknori.de/2020-07-09/issue-when-trying-to-bind-nginx-on-centos-7-4-to-other-port-than-80/
- https://upcloud.com/community/tutorials/configure-iptables-centos/
- https://developers.redhat.com/blog/2020/08/18/-iptables-the-two-variants-and-their-relationship-with-nftables#summary
- https://www.theurbanpenguin.com/using-nftables-in-centos-8/
- https://medium.com/@saiful103a/create-free-ubuntu-vps-in-oraclecloud-with-nginx-always-free-f07d9d7fad40
- https://serverfault.com/questions/985895/how-to-setup-nginx-apache-on-oracle-cloud-instance
