require 'sinatra/base'
require 'json'
require 'sqlite3'
require 'pony'
require 'sinatra/cors'

class Registration < Sinatra::Base

  set :allow_origin, "*"
  set :allow_methods, "GET,POST"
  set :allow_headers, "content-type"
  set :expose_headers, "location,link"
  set :allow_credentials, "false"

  begin
    if File.exist?('registration.db')
    else
      db = SQLite3::Database.new('registration.db')
      db.execute("CREATE TABLE IF NOT EXISTS registered (
        name varchar(100), 
        email varchar(100), 
        countrycode integer, 
        telephone integer, 
        affiliation varchar(200)
      )")
      db.close 
    end
  end

  get '/' do
    'African Ruby Mini-Conference 2021! Karibu jamii ya Ruby ya Afrika'
  end

  post '/register', provides: ['json'] do
    request.body.rewind
    request_payload = JSON.parse(request.body.read)
    # Check have name, email and telephone to be able to register
    if request_payload['email'] != nil && request_payload['email'] != "" &&
      request_payload['telephone'] != nil && request_payload['telephone'] != "" &&
      request_payload['name'] != nil && request_payload['name'] != "" &&
      request_payload['countrycode'] != nil && request_payload['countrycode'] != "" &&
      request_payload['robots'] == ""
      name = request_payload['name']
      telephone = request_payload['telephone']
      countrycode = request_payload['countrycode']
      email = request_payload['email']
      affiliation  = request_payload['affiliation']
      # Check email address not already registered
      db = SQLite3::Database.open 'registration.db'
      already_present = db.execute("SELECT email FROM registered WHERE email = ?;", email)
      if already_present[0] == [email]
        out = {message: "That email address is already registered.", success: false}.to_json
      else
        db.execute("INSERT INTO registered (name, email, countrycode, telephone, affiliation) 
                    VALUES (?, ?, ?, ?, ?)", [name, email, countrycode, telephone, affiliation])
        out = {message: "Thanks for registering, you will receive an email confirmation from a human in one business day",
               success: true}.to_json
      # confirmation message
      conf_message = 'Dear ' + name + ',\n\nThanks for registering for the conference, this is an automated message for initial confirmation, an email message from a human will follow.\n Regards,\nThe organizing committee.'
      # Send email confirmation
#       Pony.mail({:to => email, 
#                 :cc => 'organizers@nairuby.org', 
#                 :from => 'conference@nairuby.org', 
#                 :subject => 'African Ruby Mini-Conference 2021', 
#                 :body => conf_message
#                 :via => :smtp,
#                 :via_options => {
#                   :address              => 'smtp.gmail.com',
#                   :port                 => '587',
#                   :enable_starttls_auto => true,
#                   :user_name            => 'user',
#                   :password             => 'password_see_note',
#                   :authentication       => :plain, # :plain, :login, :cram_md5, no auth by default
#                   :domain               => "localhost.localdomain" # the HELO domain provided by the client
#                 }
#})
      end
      db.close
    else
      out = {message: "Your email, telephone and name are needed to register.", success: false}.to_json
    end
    return out
  end

end

